import { useState } from 'react'
import { useNavigate } from "react-router-dom";
import UsersForm from "./UsersForm.jsx";

function UsersNew() {
  const navigate = useNavigate();

  const [user, setUser] = useState({
    name: "",
  });

  const handleInputChange = (name, value) => {
    setUser((prevProps) => ({
      ...prevProps,
      [name]: value
    }));
  };

  const handleSubmit = () => {
    fetch(
      `${import.meta.env.VITE_CORE_API}/api/users`,
      {
        method: "POST",
        body: JSON.stringify({ user: user }),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      }
    ).then(() => navigate("/users"))
  };

  return (
    <>
      <UsersForm user={user} onChange={handleInputChange} onSubmit={handleSubmit}/>
    </>
  )
}

export default UsersNew
