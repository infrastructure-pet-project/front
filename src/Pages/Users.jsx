import { useState, useEffect } from 'react'
import { Link } from "react-router-dom";

function Users() {
  const [users, setUsers] = useState([])

  useEffect(() => {
    fetch(`${import.meta.env.VITE_CORE_API}/api/users`)
      .then(response => response.json())
      .then(jsonBody => setUsers(jsonBody.users))
  }, []);

  return (
    <>
      <h1>Users</h1>
      <h2>
        <Link to="/users/new">New</Link>
      </h2>
      {users.map((user) => (
        <div key={user.id}>
          <Link to={`/users/${user.id}/edit`}>{user.name}</Link>
        </div>
      ))}
    </>
  )
}

export default Users
