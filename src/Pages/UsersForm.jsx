import PropTypes from "prop-types";

function UsersForm(props) {
  const { user, onChange, onSubmit } = props;

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    onChange(name, value)
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    onSubmit()
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Name</label>
          <input
            type="text"
            name="name"
            value={user.name}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label></label>
          <button type="submit">Submit</button>
        </div>
      </form>
    </>
  )
}

UsersForm.propTypes = {
  user: PropTypes.object,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
}

export default UsersForm
