import { useState, useEffect } from 'react'
import { useNavigate, useParams } from "react-router-dom";
import UsersForm from "./UsersForm.jsx";

function UsersEdit() {
  const params = useParams();
  const navigate = useNavigate();

  const [user, setUser] = useState({
    name: "",
  });

  const handleInputChange = (name, value) => {
    setUser((prevProps) => ({
      ...prevProps,
      [name]: value
    }));
  };

  useEffect(() => {
    fetch(`${import.meta.env.VITE_CORE_API}/api/users/${params.id}`)
      .then(response => response.json())
      .then(jsonBody => setUser(jsonBody.user))
  }, [params.id]);

  const handleSubmit = () => {
    fetch(
      `${import.meta.env.VITE_CORE_API}/api/users/${params.id}`,
      {
        method: 'PATCH',
        body: JSON.stringify({ user: user }),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      }
    ).then(() => navigate("/users"))
  };

  return (
    <>
      <UsersForm user={user} onChange={handleInputChange} onSubmit={handleSubmit}/>
    </>
  )
}

export default UsersEdit
