import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import ErrorPage from "./Pages/ErrorPage.jsx";
import Users from "./Pages/Users.jsx";
import UsersNew from "./Pages/UsersNew.jsx";
import UsersEdit from "./Pages/UsersEdit.jsx";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App/>,
    errorElement: <ErrorPage />,
  },
  {
    path: "users",
    element: <Users />,
  },
  {
    path: "users/new",
    element: <UsersNew />,
  },
  {
    path: "users/:id/edit",
    element: <UsersEdit />,
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>,
)
